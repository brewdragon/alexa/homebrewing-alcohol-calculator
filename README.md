# Homebrewing Alcohol Calculator
This skill will calculate the alcohol content (ABV) of beer, mead, sake, wine,
etc. based on the beginning and ending specific gravity, brix or plato.

The skill works in conversational mode and will prompt you for input. Ask
Alexa to open the skill with "Alexa, open alc calc" and then just say
"calculate" and she'll ask you for each input value.

You can also change the units between SG, brix and plato by saying "change
units." The units setting will be retained across invocations of the skill.

Note that all plato and brix calculations will be converted to/from SG to
perform the actual ABV calculation. The "Lincoln" formula is used for unit
conversions.

The following formula is used to determine ABV:

> ABV = (76.08 * (OG-FG) / (1.775-OG)) * (FG / 0.794)

## Invocations
Example phrases to open the skill:

> *"Alexa, open Alcohol Calculator"*

> *"Alexa, open alc calc"*

Example phrases while using the skill:

> *"help"*

> *"calculate"*

> *"change units"*

> *"change units to plato"*

> *"change units to specific gravity"*

> *"change units to brix"*

> *"exit"*

> *"cancel"*

## Cards
The text of each fact will printed to a "card" in the Alexa app on your mobile
device. To view the card:

1. Open the Alexa app on your mobile device.
2. Tap the "More" button at the bottom right of the screen.
3. Select "Activity History"

The card should now be visible on your screen.

## Support
For bugs, feature requests, etc. you can [open an issue](https://gitlab.com/brewdragon/alexa/homebrewing-alcohol-calculator/-/issues)
